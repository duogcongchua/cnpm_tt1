package com.em.tt1.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class initModelMapper {

    @Bean
    public ModelMapper initMapper(){
        return new ModelMapper();
    }
}
