package com.em.tt1.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

public interface IAccountAdminService extends UserDetailsService {
}
