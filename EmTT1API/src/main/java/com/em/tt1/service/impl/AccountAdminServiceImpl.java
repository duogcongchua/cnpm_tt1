package com.em.tt1.service.impl;

import com.em.tt1.entity.AccountAdmin;
import com.em.tt1.repo.IAccountAdminRepo;
import com.em.tt1.service.IAccountAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AccountAdminServiceImpl implements IAccountAdminService {

    @Autowired
    private IAccountAdminRepo repo;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AccountAdmin account = repo.getAccountAdminByAccountUser(username);
        if (account == null){
            throw new UsernameNotFoundException(username);
        }
        return new User(account.getAccountUser(),
                account.getAccountPassword(),
                AuthorityUtils.createAuthorityList(account.getAccountRole().toString()));
    }
}
