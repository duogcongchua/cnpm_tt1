package com.em.tt1.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ACCOUNT_ADMIN",catalog = "EM_TT1")
@DynamicInsert
@Data
public class AccountAdmin implements Serializable {

    @Id
    @Column(name = "ACCOUNT_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer accountId;

    @Column(name = "ACCOUNT_USER",length = 50,nullable = false,unique = true)
    private String accountUser;

    @Column(name = "ACCOUNT_PASSWORD",length = 800,nullable = false)
    private String accountPassword;

    @Column(name = "ACCOUNT_ROLE",length = 100,nullable = false)
    private String accountRole;
}
