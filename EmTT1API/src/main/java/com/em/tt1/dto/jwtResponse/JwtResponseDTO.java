package com.em.tt1.dto.jwtResponse;

import lombok.Data;
import lombok.NonNull;

@Data
public class JwtResponseDTO {

    @NonNull
    private String token;

    @NonNull
    private String accountUser;

    @NonNull
    private String accountRole;
}
