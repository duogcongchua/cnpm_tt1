package com.em.tt1.dto.loginDTO;

import lombok.Data;

@Data
public class SignInDTO {

    private String accountUser;

    private String accountPassword;
}
