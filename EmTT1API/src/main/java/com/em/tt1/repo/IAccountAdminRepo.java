package com.em.tt1.repo;

import com.em.tt1.entity.AccountAdmin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IAccountAdminRepo extends JpaRepository<AccountAdmin,Integer>, JpaSpecificationExecutor<AccountAdmin> {

    public AccountAdmin getAccountAdminByAccountUser(String username);
}
