package com.em.tt1.controller.authController;

import com.em.tt1.dto.jwtResponse.JwtResponseDTO;
import com.em.tt1.dto.loginDTO.SignInDTO;
import com.em.tt1.service.IAccountAdminService;
import com.em.tt1.untils.JWTUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
@CrossOrigin("*")
@Validated
public class AuthController {

    @Autowired
    private JWTUtils jwtUtils;

    @Autowired
    private AuthenticationManager manager;


    @PostMapping("/signIn")
    public ResponseEntity<?> signIn(@RequestBody @Valid SignInDTO signInDTO){
        System.out.println(signInDTO.toString());
        Authentication authentication = manager.authenticate(
                new UsernamePasswordAuthenticationToken(signInDTO.getAccountUser(),signInDTO.getAccountPassword())
        );
        System.out.println(authentication);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwtToken = jwtUtils.generateJwtToken(authentication);

        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return ResponseEntity.ok(new JwtResponseDTO(
                jwtToken, userDetails.getUsername(),userDetails.getAuthorities().toString()
        ));
    }
}
